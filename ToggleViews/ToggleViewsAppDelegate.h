//
//  ToggleViewsAppDelegate.h
//  ToggleViews
//
//  Created by shashi kumar on 2/24/14.
//  Copyright (c) 2014 HCS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ToggleViewsAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
