//
//  main.m
//  ToggleViews
//
//  Created by shashi kumar on 2/24/14.
//  Copyright (c) 2014 HCS. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ToggleViewsAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ToggleViewsAppDelegate class]));
    }
}
